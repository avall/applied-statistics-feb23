{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "3f1c51a4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "*** Welcome to AppliedStatistics ***\n",
      "\n",
      "Author: Angelo Valleriani\n",
      "Affiliation: Max Planck Institute of Colloids and Interfaces\n",
      "Contact: angelo.valleriani@mpikg.mpg.de\n",
      "License: unlicensed (http://unlicense.org/)\n",
      "\n",
      "Best use as: >>>import AppliedStatistics as appst\n",
      "\n",
      "You are using AppliedStatistics version 2.1.2\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import sys # insert here the directory in which you have saved the file AppliedStatistics.py\n",
    "sys.path.append(r\"C:\\Users\\angelo\\Documents\\Python Scripts\\AppliedStatisticsRoutines\")\n",
    "sys.path.append(r\"W:\\TH-Valleriani\\AngeloSynchFold\\Python Scripts\\AppliedStatisticsRoutines\")\n",
    "import AppliedStatistics as appst\n",
    "import importlib"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93392b76",
   "metadata": {},
   "source": [
    "#### References:\n",
    "\n",
    "All exercise and data are taken from: \n",
    "\n",
    "Hogg, Tanis, Zimmerman, \"Probability and Statistical Inference\", Tenth Edition, Pearson"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e0a47cc",
   "metadata": {},
   "source": [
    "## Interval Estimation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c803d9fe",
   "metadata": {},
   "source": [
    "### Confidence interval for one proportion\n",
    "\n",
    "The standard method mostly used to compute the confidence interval for one proportion is based on the approximation of the binomial distribution with the normal distribtion. One can use this approximation when $\\hat{p}N>10$ and $(1-\\hat{p})N > 10$, where $\\hat{p}$ is the sampled proportion and $N$ is the sample size. \n",
    "\n",
    "In the binomial distribution, the items (in this case the students) are either positive (True or equal 1) or negative (False or equal 0), then if $p$ is the probability that a randomly chosen item to be 1, the random number $X$ of ones in a sample of $N$ individuals is given by the binomial distribution\n",
    "\n",
    "$$ \\Pr\\{X=k\\}\\, =\\, \\left( \\begin{array}{c} N\\\\ k \\end{array}\\right) p^k (1-p)^{N-k}\\, , $$\n",
    "\n",
    "which has the property based on the Central Limit Theorem that in the large $N$ regime it can be approximated with a normal distribution with mean $\\mu=Np$ and variance $\\sigma^2 = N p (1-p)$. \n",
    "\n",
    "Thus, under the normal approximation if $p$ is the population parameter, the sample distribution for the proportion $P$ is given by\n",
    "\n",
    "$$ P\\, =\\, \\frac{X}{N}\\, \\sim \\, N\\left(p,\\frac{p(1-p)}{N}\\right)\\, . $$\n",
    "\n",
    "Under this approximation, we therefore have \n",
    "\n",
    "$$ \\Pr\\left\\{ -z_{\\alpha/2} \\le \\frac{P-p}{\\sqrt{p(1-p)/N}} \\le z_{\\alpha/2} \\right\\}\\, =\\, 1 - \\alpha\\, ,$$\n",
    "\n",
    "because $\\frac{P-p}{\\sqrt{p(1-p)/N}}$ is normally distributed with mean $0$ and variance $1$. From this we derive a $(1-\\alpha)100$% confidence interval after the measurement of $\\hat{p} = x/N$ as\n",
    "\n",
    "$$\\hat{p}\\, \\pm \\, z_{\\alpha/2} \\sqrt{\\hat{p} (1-\\hat{p})/N}\\, ,$$\n",
    "\n",
    "where in the standard error we have substituted $p$ with $\\hat{p}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9b83b16",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "<b>Up to you:</b> The following piece of theory teaches how to compute the exact confidence interval numerically. This method holds without restrictions.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "54d09894",
   "metadata": {},
   "source": [
    "Let us suppose that $n$ is the number of positives out of a sample of $N$. For any value of the parameter $p$, the probability to observe $n$ or less positives out of $N$ is given by\n",
    "\n",
    "$$\\Pr\\{X\\le n\\}\\, =\\, \\sum_{k=0}^n \\left( \\begin{array}{c} N\\\\ k \\end{array}\\right) p^k (1-p)^{N-k}\\, .$$\n",
    "\n",
    "This probability will be small for _large_ $p$. Thus we fit the above equation to find the upper boundary $p_U$ of the confidence interval by just looking at the value of $p$ such that \n",
    "\n",
    "$$\\Pr\\{X\\le n\\} \\, =\\, \\frac{\\alpha}{2} \\, .$$\n",
    "\n",
    "On the other hand, the probability \n",
    "\n",
    "$$\\Pr\\{X\\ge n\\}\\, =\\, \\sum_{k=n}^N \\left( \\begin{array}{c} N\\\\ k \\end{array}\\right) p^k (1-p)^{N-k}\\, $$\n",
    "\n",
    "will be small for _small_ $p$. Thus we fit the above equation to find the lower boundary $p_L$ of the confidence interval by just looking at the value of $p$ such that \n",
    "\n",
    "$$\\Pr\\{X\\ge n\\} \\, =\\,  \\frac{\\alpha}{2} \\, .$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "634eb0c9",
   "metadata": {},
   "source": [
    "#### Exercise 13"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d53ad2f3",
   "metadata": {},
   "source": [
    "Health officials are worried about the rate of vaccination of children against DPT (diphtheria, pertussis, tetanus). They decide that if the proportion of immunized children is below 90% they would start an expensive immunization campaign.\n",
    "\n",
    "A sample of 𝑁 = 537 children was checked to find out that 𝑥 = 460 had been immunized.  We seek for the 95% confidence interval for the proportion of immunized children in the country."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "670bf8f2",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "z_a2 = 1.96\n",
      "Applying the normal approximation\n",
      "The point estimate of the proportion is: 0.857\n",
      "The point estimate of the associated standard deviation is: 0.015\n",
      "\n",
      "The two-sided 95.0% confidence interval for the population proportion p is:\n",
      "[0.827, 0.886]\n"
     ]
    }
   ],
   "source": [
    "#7.3-5\n",
    "from scipy.stats import norm\n",
    "alpha = 0.05\n",
    "x = 460\n",
    "N = 537\n",
    "z_a2 = -norm.ppf(alpha/2)\n",
    "print(\"z_a2 =\", round(z_a2,3))\n",
    "p_hat = x/N\n",
    "print(\"Applying the normal approximation\")\n",
    "print(\"The point estimate of the proportion is:\", round(p_hat,3))\n",
    "std = np.sqrt(p_hat*(1-p_hat)/N)\n",
    "print(\"The point estimate of the associated standard deviation is:\", round(std,3))\n",
    "lower_p = p_hat - z_a2*std\n",
    "upper_p = p_hat + z_a2*std\n",
    "print()\n",
    "print(\"The two-sided \"+str((1-alpha)*100)+\"% confidence interval for the population proportion p is:\")\n",
    "print([round(lower_p,3), round(upper_p,3)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "6a54aed5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "The two-sided 95.0% confidence interval for the population proportion p is:\n",
      "[0.83, 0.89]\n"
     ]
    }
   ],
   "source": [
    "# using the approximate numbers in the lecture we have\n",
    "lower_p = 0.857 - 1.96*0.015\n",
    "upper_p = 0.857 + 1.96*0.015\n",
    "print()\n",
    "print(\"The two-sided \"+str((1-alpha)*100)+\"% confidence interval for the population proportion p is:\")\n",
    "print([round(lower_p,2), round(upper_p,2)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "7ebc28db",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "z_a = 1.645\n",
      "\n",
      "The right-sided 95.0% confidence interval for the population proportion p is:\n",
      "[0, 0.88]\n"
     ]
    }
   ],
   "source": [
    "# looking for the upper bound\n",
    "z_a = -norm.ppf(alpha)\n",
    "print(\"z_a =\", round(z_a,3))\n",
    "lower_p = 0\n",
    "upper_p = p_hat + z_a*std\n",
    "print()\n",
    "print(\"The right-sided \"+str((1-alpha)*100)+\"% confidence interval for the population proportion p is:\")\n",
    "print([round(lower_p,2), round(upper_p,2)])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46fafc3a",
   "metadata": {},
   "source": [
    "Here the results using the routine in appst."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "3b3ac31e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The exact, two-sided 95.0 % confidence interval for the population proportion is given by [0.82, 0.89]\n",
      "The exact, two-sided 95.0 % confidence interval computed with the beta-distribution is given by [0.82, 0.89]\n",
      "The point estimate of the proportion is: 0.86\n",
      "The approximate z- 95.0 % confidence interval for the population proportion is given by [0.83, 0.89]\n",
      "The point estimate of the proportion is: 0.86\n",
      "The s- 95.0 % confidence interval for the population proportion is given by [0.82, 0.88]\n",
      "The point estimate of the proportion is: 0.85\n",
      "The Agresti-Coull 95.0 % confidence interval for the population proportion is given by [0.82, 0.88]\n"
     ]
    }
   ],
   "source": [
    "results = appst.conf_int_for_one_prop(n=x, N=N, sides = 'two', prec = 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "e7437fab",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The exact, right-sided 95.0 % confidence interval for the population proportion is given by [0.0, 0.88]\n",
      "The exact, right-sided 95.0 % confidence interval computed with the beta-distribution is given by [0.0, 0.88]\n",
      "The point estimate of the proportion is: 0.86\n",
      "The approximate z- 95.0 % confidence interval for the population proportion is given by [0, 0.88]\n"
     ]
    }
   ],
   "source": [
    "results = appst.conf_int_for_one_prop(n=x, N=N, sides = 'right', prec = 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6724b0d4",
   "metadata": {},
   "source": [
    "### Estimation of the difference of two proportions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d8980db",
   "metadata": {},
   "source": [
    "Let us assume that $P_x$ and $P_y$ are the sample proportions with sample sizes of $N_x$ and $N_y$, respectively. If the proportions are not too small and the sample sizes are not too small, the sample proportions are approximately normally distributed with mean $p_x$ and $p_y$ (i.e. the population proportions) and variances $\\sigma^2_x=p_x (1-p_x)/N_x$ and $\\sigma^2_y=p_y (1-p_y)/N_y$ respectively.\n",
    "\n",
    "Therefore, the random variable\n",
    "$$\n",
    "Z\\, =\\, \\frac{\\left(P_x-P_y\\right) - (p_x-p_y)}{\\sqrt{\\sigma^2_x + \\sigma^2_y}}\n",
    "$$\n",
    "is normally distributed with mean zero and variance 1.\n",
    "\n",
    "In analogy with the t-interval, we have also here that a number $z_{\\alpha/2}$ exists such that\n",
    "\n",
    "$$\n",
    "\\Pr\\left\\{-z_{\\alpha/2}\\le Z\\le z_{\\alpha/2}\\right\\} \\, =\\, 1-\\alpha\\, .\n",
    "$$\n",
    "\n",
    "Once we have taken the samples and we compute the proportions $\\hat{p}_x$ and $\\hat{p}_y$ from the data, the confidence interval for $p_x-p_y$ is given by\n",
    "\n",
    "$$\n",
    "\\hat{p}_x - \\hat{p}_y \\, \\pm\\, z_{\\alpha/2}\\sqrt{s^2_x + s^2_y}\n",
    "$$\n",
    "\n",
    "with $s^2_x=\\hat{p}_x (1-\\hat{p}_x)/N_x$ and $s^2_y=\\hat{p}_y (1-\\hat{p}_y)/N_y$. \n",
    "\n",
    "Note that there are other methods to compute the confidence interval for the difference of two proportions. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6e754b8",
   "metadata": {},
   "source": [
    "#### Exercise 14"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a5097c2",
   "metadata": {},
   "source": [
    "Consider two group of people. Group 1 comprises people who spend less than 500 EUR annually on cloths. Group 2 consists of people who spend more than 1000 EUR annually on clothes.\n",
    "\n",
    "Within each group, a certain proportion of people believes that clothes are too expensive. Say that these proportions are $𝑝_1$ and $𝑝_2$ in Group 1 and in Group 2, respectively.\n",
    "\n",
    "Out of two samples of size 𝑁 = 1230 for Group 1 and 𝑀 = 340 for Group 2, the number of people who believe that clothes are too expensive was found to be $𝑥_1=1009$ and $𝑥_2=207$.\n",
    "\n",
    "We seek for the 95% confidence interval for $𝑝_1−𝑝_2$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "edd77fb1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "z_a2 = 1.96\n"
     ]
    }
   ],
   "source": [
    "# 7.3.9\n",
    "from scipy.stats import norm\n",
    "alpha = 0.05\n",
    "z_a2 = -norm.ppf(alpha/2)\n",
    "print(\"z_a2 =\", round(z_a2,3))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "33ddb6a0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The point estimate of the proportion in Group 1 is p1 = 0.82\n",
      "The point estimate of the proportion in Group 2 is p2 = 0.609\n",
      "The point estimate of the difference p1-p2 of the two proportion = 0.212\n"
     ]
    }
   ],
   "source": [
    "N = 1230\n",
    "M = 340\n",
    "x1 = 1009\n",
    "x2 = 207\n",
    "p1_hat = x1/N\n",
    "p2_hat = x2/M\n",
    "print(\"The point estimate of the proportion in Group 1 is p1 =\", round(p1_hat,3))\n",
    "print(\"The point estimate of the proportion in Group 2 is p2 =\", round(p2_hat,3))\n",
    "print(\"The point estimate of the difference p1-p2 of the two proportion =\", round(p1_hat-p2_hat,3))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "4454b561",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The common variance is: 0.001\n",
      "The error of the estimate is: 0.056\n",
      "The two-sided 95.0% confidence interval for the difference is: [0.155, 0.268]\n"
     ]
    }
   ],
   "source": [
    "var = p1_hat*(1-p1_hat)/N + p2_hat*(1-p2_hat)/M\n",
    "print(\"The common variance is:\", round(var,3))\n",
    "err = z_a2*np.sqrt(var)\n",
    "print(\"The error of the estimate is:\", round(err, 3))\n",
    "lower_bound = p1_hat-p2_hat - err\n",
    "upper_bound = p1_hat-p2_hat + err\n",
    "print(\"The two-sided \"+str((1-alpha)*100)+\"% confidence interval for the difference is:\",\\\n",
    "      [round(lower_bound,3), round(upper_bound,3)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "86dc4a70",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The point estimate of the first proportion is: 0.82\n",
      "The point estimate of the second proportion is: 0.609\n",
      "The point estimate of the difference, p1-p2, is: 0.212\n",
      "The approximate z- 95.0 % confidence interval for the difference of the proportions is given by [0.155, 0.268]\n",
      "The 95.0 % confidence interval for the difference of the proportions with resampling is given by [0.157, 0.269]\n"
     ]
    }
   ],
   "source": [
    "results = appst.conf_int_for_two_prop(n1=x1, N1=N, n2=x2,N2=M,alpha=0.05, sides = 'two', \\\n",
    "                                resampling=True, verbose = True)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fc526f1",
   "metadata": {},
   "source": [
    "#### Exercise 15"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58989615",
   "metadata": {},
   "source": [
    "In a survey, people were asked to say what they believe is the major factor of pollution among three choices: “automobiles”, “factories” or “incinerators”.\n",
    "\n",
    "To check for bias effect, two formulations, A and B, of the questionnaire were distributed. Let $𝑝_𝐴$ and $𝑝_𝐵$ be the proportions of people who think that “factories” are the main pollution factor when reading questionnaire A and B, respectively.\n",
    "\n",
    "A sample of 𝑁 = 460 people got questionnaire A and $𝑥_1=170$ chose “factories”. For questionnaire B we have instead $𝑥_2=140$ out of 𝑀 = 440 respondents. \n",
    "\n",
    "Let us find a two-sided 95% confidence interval for $𝑝_𝐴−𝑝_𝐵$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "f1be6a81",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "z_a2 = 1.96\n"
     ]
    }
   ],
   "source": [
    "# 7.3-12\n",
    "from scipy.stats import norm\n",
    "alpha = 0.05\n",
    "z_a2 = -norm.ppf(alpha/2)\n",
    "print(\"z_a2 =\", round(z_a2,3))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "a19c4cd6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The point estimate of the proportion in Group 1 is p1 = 0.37\n",
      "The point estimate of the proportion in Group 2 is p2 = 0.318\n",
      "The point estimate of the difference p1-p2 of the two proportion = 0.051\n"
     ]
    }
   ],
   "source": [
    "N = 460\n",
    "M = 440\n",
    "x1 = 170\n",
    "x2 = 140\n",
    "p1_hat = x1/N\n",
    "p2_hat = x2/M\n",
    "print(\"The point estimate of the proportion in Group 1 is p1 =\", round(p1_hat,3))\n",
    "print(\"The point estimate of the proportion in Group 2 is p2 =\", round(p2_hat,3))\n",
    "print(\"The point estimate of the difference p1-p2 of the two proportion =\", round(p1_hat-p2_hat,3))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "1c158891",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The common variance is: 0.001\n",
      "The error of the estimate is: 0.062\n",
      "The two-sided 95.0% confidence interval for the difference is: [-0.011, 0.113]\n"
     ]
    }
   ],
   "source": [
    "var = p1_hat*(1-p1_hat)/N + p2_hat*(1-p2_hat)/M\n",
    "print(\"The common variance is:\", round(var,3))\n",
    "err = z_a2*np.sqrt(var)\n",
    "print(\"The error of the estimate is:\", round(err, 3))\n",
    "lower_bound = p1_hat-p2_hat - err\n",
    "upper_bound = p1_hat-p2_hat + err\n",
    "print(\"The two-sided \"+str((1-alpha)*100)+\"% confidence interval for the difference is:\",\\\n",
    "      [round(lower_bound,3), round(upper_bound,3)])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "dbb6c120",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The point estimate of the first proportion is: 0.37\n",
      "The point estimate of the second proportion is: 0.318\n",
      "The point estimate of the difference, p1-p2, is: 0.051\n",
      "The approximate z- 95.0 % confidence interval for the difference of the proportions is given by [-0.011, 0.113]\n",
      "The 95.0 % confidence interval for the difference of the proportions with resampling is given by [-0.01, 0.114]\n"
     ]
    }
   ],
   "source": [
    "results = appst.conf_int_for_two_prop(n1=x1, N1=N, n2=x2,N2=M,alpha=0.05, sides = 'two', \\\n",
    "                                resampling=True, verbose = True)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "bae75e6e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The point estimate of the first proportion is: 0.37\n",
      "The point estimate of the second proportion is: 0.318\n",
      "The point estimate of the difference, p1-p2, is: 0.051\n",
      "The approximate z- 95.0 % confidence interval for the difference of the proportions is given by [-0.145, 0.247]\n",
      "The 95.0 % confidence interval for the difference of the proportions with resampling is given by [-0.148, 0.25]\n"
     ]
    }
   ],
   "source": [
    "# how does the confidence interval change if the numbers involved are one tenth?\n",
    "results = appst.conf_int_for_two_prop(n1=17, N1=46, n2=14,N2=44,alpha=0.05, sides = 'two', \\\n",
    "                                resampling=True, verbose = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "622b51b0",
   "metadata": {},
   "source": [
    "## Sample sizes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3636e2dd",
   "metadata": {},
   "source": [
    "#### Exercise 16"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ebc3fc3",
   "metadata": {},
   "source": [
    "Suppose that we want to estimate the mean length of the tarsus of a male grackle. We assume that the variance is known (e.g. from previous studies) and it is $\\sigma^2=4.84$ cm$^2$. \n",
    "\n",
    "We want to determine the sample size 𝑁 for the measurements, so that the maximal error in a two-sided 95% confidence interval is $\\epsilon=0.4$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "bb1aaf31",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "z_a2 = 1.96\n",
      "The standard deviation is sigma = 2.2\n",
      "The required sample size is N = 117\n"
     ]
    }
   ],
   "source": [
    "# 7.4.1\n",
    "from scipy.stats import norm\n",
    "alpha = 0.05\n",
    "z_a2 = -norm.ppf(alpha/2)\n",
    "print(\"z_a2 =\", round(z_a2,3))\n",
    "sigma2 = 4.84\n",
    "sigma = np.sqrt(sigma2)\n",
    "print(\"The standard deviation is sigma =\",round(sigma, 3))\n",
    "eps = 0.4\n",
    "N = int((z_a2*sigma/eps)**2)+1\n",
    "print(\"The required sample size is N =\", N)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "d45f620a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "116.20839999999998\n",
      "The required sample size is N = 117\n"
     ]
    }
   ],
   "source": [
    "# using the approximate values in the slides\n",
    "N = (1.96*2.2/0.4)**2\n",
    "print(N)\n",
    "print(\"The required sample size is N =\", int(N)+1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "6d6a81db",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Compute the sample size for the required maximum error, $\\epsilon$ = 0.4\n",
      "The maximum error is set equal to the half of the two-sided confidence interval.\n",
      "\n",
      "The calculation is based on the z-interval estimate\n",
      "The resulting sample size is N = 117\n"
     ]
    }
   ],
   "source": [
    "N = appst.sample_size_one_mean(sigma2 = 4.84, s2 = None, alpha = 0.05, epsilon = 0.4)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14820695",
   "metadata": {},
   "source": [
    "#### Exercise 17"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69f565c5",
   "metadata": {},
   "source": [
    "Out of 𝑁 = 137 Canadian geese, 𝑥 = 54 are infected by a microorganism. \n",
    "\n",
    "The researchers are interested in giving an estimate of the proportion $𝑝$ of infected geese so that the maximum error in a two-sided 90% confidence interval is $\\epsilon = 0.04$. \n",
    "\n",
    "What is the number of geese they would have to test for the microorganisms in a follow-up study?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "id": "ba4e36f4",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "z_a2 = 1.645\n"
     ]
    }
   ],
   "source": [
    "from scipy.stats import norm\n",
    "alpha = 0.1\n",
    "z_a2 = -norm.ppf(alpha/2)\n",
    "print(\"z_a2 =\", round(z_a2,3))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "id": "b17b5e84",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "p_hat = 0.394\n",
      "\n",
      "The current error of the estimate is approximately: 0.069\n",
      "To achieve an error of 0.04, a sample size N = 404 is required\n",
      "\n",
      "To achieve an error of 0.04, a sample size N = 423 is certainly sufficient\n"
     ]
    }
   ],
   "source": [
    "x = 54\n",
    "N = 137\n",
    "eps = 0.04\n",
    "p_hat = x/N\n",
    "print(\"p_hat =\", round(p_hat,3))\n",
    "print()\n",
    "variance = p_hat*(1-p_hat)/N\n",
    "std = np.sqrt(variance)\n",
    "current_error = z_a2*std\n",
    "print(\"The current error of the estimate is approximately:\", round(current_error,3))\n",
    "N = (z_a2*np.sqrt(p_hat*(1-p_hat))/eps)**2\n",
    "N = int(N) + 1\n",
    "print(\"To achieve an error of \"+str(eps)+\", a sample size N =\", N, \"is required\")\n",
    "print()\n",
    "N_max = (z_a2*np.sqrt(0.5*(1-0.5))/eps)**2\n",
    "N_max = int(N_max)+1\n",
    "print(\"To achieve an error of \"+str(eps)+\", a sample size N =\", N_max, \"is certainly sufficient\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "id": "4dffca43",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Compute the sample size for the required maximum error, $\\epsilon$ = 0.04\n",
      "The maximum error is set equal to the half of the two-sided confidence interval.\n",
      "\n",
      "The calculation is based on the normal approximation for proportions.\n",
      "\n",
      "The sample size is: 404\n"
     ]
    }
   ],
   "source": [
    "N = appst.sample_size_one_prop(p=54/137, alpha=0.1, epsilon = 0.04, popsize=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3470a229",
   "metadata": {},
   "source": [
    "## Conclusions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "470f5a67",
   "metadata": {},
   "source": [
    "From this unit you have learned how to estimate the population proportion. We have seen the method based on the normal approximation. Other methods, are available in the routines indecated here. When the sample sizes are large, all methods deliver very similar results. For small sample size, when the Central Limit Theorem does not hold so well, other methods are more indicated. The routines provided here deliver several estimates based on different methods. The \"exact\" estimate is probably the most accurate, though most people would agree with the normality assumption in most cases.\n",
    "\n",
    "When comparing two proportions, we mostly use the normal approximation. Also here the sample sizes need to be relatively large. When the sample sizes are small, it is most appropriate to use some resampling method. The routine appst.conf_int_for_two_prop provides this possibility. \n",
    "\n",
    "The determination of the sample size before making an observation is important in order to achieve a certain level of maximal error. Usually, this kind of calculation can be made more precise if there is some previous knowledge about the system, such as the expected variance or the proportion. The examples provided here for the mean are based on the z-interval. The routine allows to determine the sample size also when the use of the t-interval is more appropriate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e3b0a822",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
